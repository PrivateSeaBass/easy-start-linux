#/bin/bash

#Created by Private SeaBass.
#Gitlab: 
# https://gitlab.com/PrivateSeaBass/easy-start-linux
#
#This is a basic setup script for setting up a debian based computer
#Part of the purpose is to allow for someone who 
# would like an easier personalizable startup for Linux.
#In light of that:
# grab this script, 
# rename it, 
# and store it with personally prefered basic applications for any time you flash linux!
#
#
# These applications are known to work in debian 9 (stretch)

#============ Quick Instructions ================


# "#" on applications you do NOT want
# Remove "#" on applications you want
# Run script with "sudo"

#============ Full Instructions ================

# Add a "#" at start of line, if unwanted application
# Remove a "# if wanted application
# Save the script
# 	vim/vi 		:w	(or to save and quit :wq)
#	nano 		^x	(Control + x)
#	GUI editor	^s	(Control + s)(Usually)
# Exit text editor
# Run the script with root permissions (as root or with sudo)
# 	$(sudo) ./EasyDebian-BasedStartup.sh
#
# Sometimes I run into issues about the file not being executable.
# So, if it won't run, this might be the reason.
# Solution:
# 	$chmod u+x EasyDebian-BasedStartup.sh
#
# Try running again.

#============ Tools to pick ================


#DESKTOP ENVIRONMENTS (reccommend KDE)
#WARNING: installing both seems to prevent lock screens from working on laptop lid close for gnome
#NOTE: These are likely different in Ubuntu 
#kde="kde-plasma-desktop"
#gnome="gnome-core"
#xfce="xfce4"
#cinnamon="cinnamon"
#mate="task-mate-desktop"
#i3="i3"

#BASIC RECOMMENDATIONS
#vim="vim"
#tmux="tmux" 
#terminator="terminator"
#gnupg="gnupg2"
#fish="fish"
#zsh="zsh"

#TOOLS
#gparted="gparted"
#nmap="nmap"
#git="git"
#wget="wget"
#curl="curl"

#SECURITY ORIENTED SOFTWARE
#bleachbit="bleachbit"
#keepass="keepass2"
#veracrypt=false
#veracryptCML=false
#signal=false
#wireshark="wireshark"
#elinks="elinks"
#clamav="clamav"
#conky="conky"
#htop="htop"
#openssl="openssl"
#firejail="firejail"
#apparmor="apparmor"
#ufw="ufw"

#BLOCKCHAIN BASED
#nodejs=false
#nodejsLTS=false
#naivecoin=false

#VPN PLUG-INS
#openconnect="network-manager-openconnect"
#openconenctGnome="network-manager-openconnect-gnome"
#openvpn="network-manager-openvpn"
#openvpnGnome="network-manager-openvpn-gnome"

#ENTERTAINMENT
#cowsay="cowsay"
#matrix="cmatrix"
#sl="sl"

#ADDITIONAL CONFIGURATIONS
#Have NVidia card + intel chip and need to change your bootloader default config to accomidate?
#nvidia=true

#Want sudoers to add your account to it? (allowing current user to use the "sudo" command?)
#addSudo=true

#Want sudo to ask for root password? (as is essencially the admin password)
#DO NOT US THIS IF THERE IS NO ROOT USER (normally is the case if you didn't set a root password durring installation.) 
#sudoRoot=true

#Want simple security setup? (ufw, firejail, and ClamAV)
#securitySetup="ufw firejail clamav"


#================== End of Tools to Pick ==================
#================== Sudoers configuration =================
# Currently not successful.

verify () {
	while true; do
	read yn
	case $yn in
	       [Yy]* ) verification=true ; break;;
	       [Nn]* ) verification=false ; break;;
	       * ) echo "Please answer yes or no.";;
	esac
	done
}

if [ ! -z  ${sudoRoot+x} ] ; then
	echo "Adding option for requiring root password on use of sudo..."

	sudo cat /etc/sudoers | grep rootpw > TEMP
	
	#if not already there
	cat TEMP
	if [ ! -s TEMP ] ; then
		#add it in
		echo "Defaults        rootpw" | sudo EDITOR='tee -a' visudo 
	else
		echo "Already have it in there"
	fi
	#remove temp
	sudo rm -f TEMP 
fi

# add user to sudoers file.
if [ ! -z  ${addSudo+x} ] ; then
	echo "Username of user added to sudoers?"
	read userToAdd

	# check if user is already in sudoers and not it in TEMP
	sudo cat /etc/sudoers | grep $userToAdd > TEMP

	# if not already there, add it.
	cat TEMP
	if [ ! -s TEMP ] ; then
		echo "adding $userToAdd"
		echo "$userToAdd ALL=(ALL:ALL) ALL" | sudo EDITOR='tee -a' visudo 
	else
		echo "Already have it in there"
	fi
	
	# remove temp
	sudo rm -f TEMP 
fi

#======== Grub Default Modifications ======================
if [ ! -z  ${nvidia+x} ] ; then
	echo "Starting Grub config."
	echo "Adding default option \"nouveau.modeset=0\""

	#edit defaults to add (substitute with more) a default config parameter in the folder
	sudo sed 's/GRUB_CMDLINE_LINUX_DEFAULT=\"/GRUB_CMDLINE_LINUX_DEFAULT=\"nouveau.modeset=0 /' /etc/default/grub > /etc/default/almostGrub
	sudo rm -f /etc/default/grub
	sudo mv /etc/default/almostGrub /etc/default/grub

	#creating and setting the file that will be run on startup.
	sudo grub-mkconfig -o /boot/grub/grub.cfg

fi

#======== general updates and installs ====================

#if the user wants signal
if [ ! -z ${signal+x} ] ; then
	#notify user that signal install needs curl and https apt
	echo "You need 'curl' and 'apt-transport-https' to install signal."
	echo "Is this okay?(y/n)"
	#set verification variable
	verify

	#User is okay with extra installs
	if [ "$verification" = true ] ; then 
		#curl and https apt has to be installed
		sudo apt install curl apt-transport-https -y

		#do instructions on signal website
		curl -s https://updates.signal.org/desktop/apt/keys.asc | sudo apt-key add -
		echo "deb [arch=amd64] https://updates.signal.org/desktop/apt xenial main" | tee -a /etc/apt/sources.list.d/signal-xenial.list

		#prepare it for install
		signal="signal-desktop"
	else
		signal=' '
		
	fi
fi

#if user wants veracrypt
if [ ! -z ${veracrypt+x} ] || [ ! -z ${veracryptCML+x} ] ;  then
	#notify user that veracrypt install needs curl, wget, and gnupg2
	echo "You need 'curl', 'wget', and 'gnupg' to install VeraCrypt."
	echo "Is this okay?(y/n)"
	#set verification variable
	verify
	
	#User is okay with extra installs
	if [ "$verification" = true ] ; then 
		#set for installation
		curl="curl"
		gnupg="gnupg2"
		wget="wget"
		veracrypt=true
		veracryptCML=true
	fi
fi

#if user wants naivecoin
if [ ! -z ${naivecoin+x} ] ;  then
	#notify user that naivecoin needs nodejs
	echo "You need 'nodejs' and 'wget' to install naivecoin."
	echo "Is this okay?(y/n)"
	#Set verification variable
	verify

	#user is okay with extra
	if [ "$verification" = true ] ; then 
		#set for installation
		softwarePropertiesCommon="software-properties-common"
		wget="wget"
		nodejs=true
		nodejs=true
	fi
fi

#if user wants nodejs
if [ ! -z ${nodejs+x} ] || [ ! -z ${nodejsLTS+x} ] ;  then
	#notify user that nodejs needs curl and common software properties packages
	echo "You need 'curl' and 'software-properties-common' to install nodejs."
	echo "Is this okay?(y/n)"
	#set verification variable
	verify

	#User is okay with extra installs
	if [ "$verification" = true ] ; then 
		#set for installation
		softwarePropertiesCommon="software-properties-common"
		curl="curl"
		nodejs=true
		nodejsLTS=true
	else
		nodejs=false
		nodejsLTS=false
	fi	
fi

#basic applications for startup and updates
#can't wait on updates
sudo apt install $curl $wget $softwarePropertiesCommon $gnupg $fish -y

#if the user wants veracrypt
if [ ! -z ${veracrypt+x} ] || [ ! -z ${veracryptCML+x} ] ; then

	if [ "$veracrypt" = true ] || [ "$veracryptCML" = true ] ; then

		#get public key and import it
		curl https://www.idrix.fr/VeraCrypt/VeraCrypt_PGP_public_key.asc | gpg --import 
	
		#get veracrypt and its signature.
		wget https://launchpad.net/veracrypt/trunk/1.23/+download/veracrypt-1.23-setup.tar.bz2 https://launchpad.net/veracrypt/trunk/1.23/+download/veracrypt-1.23-setup.tar.bz2.sig
	
		#verify signature with tar file
		gpg --verify veracrypt-1.23-setup.tar.bz2.sig veracrypt-1.23-setup.tar.bz2 
	
		#have user check validity
		echo "VALID INSTALL CHECK (Check both):"
		echo "FINGERPRINT = 5069 A233 D55A 0EEB 174A 5FC3 821A CD02 680D 16DE"
		echo "IT IS A GOOD SIGNATURE FROM THE VERACRYPT TEAM."
		echo "IS THIS ACCURATE INFORMATION?[YES/NO]"
		#set verification variable
		verify

		
		#if veracrypt couldn't be verified
		if [ "$verification" = false ] ; then
			echo "IF THIS WAS NOT A MISTAKE, THIS SCRIPT CAN NOT INSTALL VERIFIED VERACRYPT VERSION."
				echo "MANUAL INSTALL NECESSARY."
			echo "IF VERIFICATION FAILS AGAIN, REPORT FALSE SIGNATURES TO VERACRYPT TEAM."
	
	
		#if it could be verified, install.
		else
			#unpack veracrypt tar file
			tar xvf veracrypt-1.23-setup.tar.bz2
	
			#check if CML version is desired
			if [ ! -z ${veracryptCML+x} ]; then
				echo "Installing commandline VeraCrypt..."
				sudo ./veracrypt-1.23-setup-console-x64
	

			#GUI version desired
			else
				echo "Installing GUI VeraCrypt..."
				sudo ./veracrypt-1.23-setup-gui-x64 
			
			fi
		
		fi
		#remove all files no longer used.
		sudo rm -rf VeraCrypt* && sudo rm -rf veracrypt*
	fi
fi

#if the user wants fish
if [ ! -z ${fish+x} ] ; then
	#change the shell of root to fish
	echo "Changing to fish for root requires root password."
	sudo chsh -s `which fish`
	
	#change the shell of user to fish
	echo "Changing your shell to fish requires user password."
	chsh -s `which fish`

fi

#can wait on updates
sudo apt update && sudo apt full-upgrade -y && sudo apt install $zsh $firejail $terminator $keepass $vim $bleachbit $openconnect $openvpn $openconnectGnome $openvpnGnome $kde $gnome $elinks $wireshark $tmux $sl $cmatrix $cowsay $nmap $gparted $git $openssl $conky $atom $htop $apparmor $clamav $ufw $xfce $cinnamon $mate $i3 $signal -y && sudo apt autoremove -y

#if we want node.js
if [ ! -z ${nodejs+x} ] || [ ! -z ${nodejsLTS+x} ] ; then
	
	if [ "$nodejs" = true ] || [ "$nodejsLTS" = true ] ; then
	
		#current version
		if [ ! -z ${nodejs+x} ] ; then
			#use online instructions
			curl -sL https://deb.nodesource.com/setup_10.x | sudo bash -
	
		#LTS version.
		else
			#use online instructions
			curl -sL https://deb.nodesource.com/setup_8.x | sudo bash -
	
		fi
	
		#install
		sudo apt install nodejs
		#update npm
		npm i npm

		#install naivecoin
		#if we want naivecoin
		if [ ! -z ${naivecoin+x} ] ; then
			#copy, unzip, and remove zip from repository.
			wget https://github.com/conradoqg/naivecoin/archive/master.zip
			unzip master.zip
			rm master.zip

			#needed npm installs
			sudo npm install express body-parser swagger-ui-express ramda elliptic es6-error statuses timeago.js fs-extra threads superagent yargs cli-color   

		fi

	fi
fi

#============== QUICK SECURITY BASED INJECTION ============================
if [ ! -z ${securitySetup+x} ] ; then
	#setup clamAV and install unofficial signatures.
	sudo freshclam
	sudo systemctl start clamav-daemon.services
	sudo systemctl enable clamav-daemon.service
	sudo apt install clamav-unofficial-sigs

    	#UFW simple setup (allow on computer startup and allow ssh.)
	sudo systemctl start ufw.service
	sudo systemctl enable ufw.service
    	sudo ufw default deny incoming
    	sudo ufw default allow outgoing
    	sudo ufw allow ssh
    	sudo ufw enable 

fi


#TODO: OpenNic(dnsdiag for diagnostics for it)
#TODO: ArpON